Create_1_role [nomKeySpace] [nomRole]

->Create one role with the right CREATE, ALTER, DROP, SELECT, MODIFY
on only one KEYSPACE link to the role. Le KEYSPACE has a replication factor of 2.

Delete_keyspace [nomKeySpace]

->Delete one KEYSPACE IF EXISTS. 

Delete_keyspace_not_empty [nomKeySpace] 

->Delete one KEYSPACE IF EXISTS and NOT EMPTY.  

List_keyspaces 

->List of all keyspaces in the cluster.

List_tables 

->List of all tables in a keyspace.

