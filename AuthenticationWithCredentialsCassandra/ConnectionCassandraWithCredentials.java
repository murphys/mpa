
package CQLOperation;
import com.datastax.oss.driver.api.core.CqlSession;
import com.datastax.oss.driver.api.core.CqlSessionBuilder;
import com.datastax.oss.driver.api.core.metadata.schema.TableMetadata;

import java.net.InetSocketAddress;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Connection to cassandra Data Base.
 */
public class ConnectionCassandraWithCredentials {

    private static final int PORT = 9042;
    private InetSocketAddress inetConnection;
    private String ipAddress;
    private CqlSession session;
    private CassandraDB cassandraDB;


    /**
     * Connect to CassandraDB
     * @param ipAddress
     * @param cassandraDB
     */
    public ConnectionCassandraWithCredentials(String ipAddress, CassandraDB cassandraDB) {
        this.inetConnection = new InetSocketAddress(ipAddress, PORT);
        this.ipAddress = ipAddress;
        this.cassandraDB = cassandraDB;
        List<String> listTables = null;

        if(!this.getCassandraDB().getListIpAddress().contains(ipAddress)){
            throw new IllegalArgumentException("the ip address is not knowing, please verif the ip address and add it to the list of ip address");
        }
        CqlSessionBuilder session =
        	    CqlSession.builder()
        	        .withAuthCredentials("admin", "admin");
        this.session = session
                .withLocalDatacenter(cassandraDB.getDataCenterName())
                .addContactPoint(inetConnection)
              
                .build();
        Collection<TableMetadata> tables = this.session.getMetadata().getKeyspace(cassandraDB.getKeySpaceName())
                .get().getTables().values();
        if(tables!=null) {
            listTables = tables.stream().map(tableMetadata -> tableMetadata.getName().toString())
                    .collect(Collectors.toList());
        }

        cassandraDB.setListTables(listTables);

    }

    /**
     * Closes the connection. If the connection is closed, he is impossible to acces cassandraDB.
     * @return boolean
     */
    public boolean closeConnection(){
        if(!session.isClosed()){
            session.close();
        }
        return session.isClosed();
    }

    /**
     * Allow to know if the session is closed or not.
     * @return boolean
     */
    public boolean isClose(){
        return session.isClosed();
    }

    /**
     * Get the InetSocketAddress instance. It's an IP Socket Address (IP address + port number).
     * @return InetSocketAddress
     */
    public InetSocketAddress getInetConnection() {
        return inetConnection;
    }

    /**
     * Get the ip address.
     * @return the ip address
     */
    public String getIpAddress() {
        return ipAddress;
    }

    /**
     * Set the Ip address.
     * @param ipAddress
     */
    public void setIpAddress(String ipAddress) {
        this.ipAddress = ipAddress;
    }

    /**
     * Get the CassandraDB.
     * @return cassandraDB
     */
    public CassandraDB getCassandraDB() {
        return cassandraDB;
    }

    /**
     * Create a String of the attributs.
     */
    @Override
    public String toString() {
        return "\nConnectionCassandra { " +
                "\n    inetConnection { \n        -> ipAddress = '" + ipAddress + '\'' +
                ",\n        -> port = " + this.PORT +
                "    }\n}";
    }

    protected CqlSession getSession() {
        return session;
    }
}
