
**Activer l'authentification Cassandra**

> modifier le fichier /etc/cassandra/cassandra.yaml :

authenticator: PasswordAuthenticator
authorizer: CassandraAuthorizer

> créer le fichier cqlshrc dans ~/.cassandra/ et ajouter un utilisateur par défaut :

[authentication]
username = rootRihab
password = rootRihab

[ui]
color = on
completekey = tab

[connection]
hostname = 172.19.28.182 
port = 9042

> se connecter à cqlsh avec credentials :

cqlsh -u rootRihab -p rootRihab

> modifier la classe ConnectionCassandra.java ===> ConnectionCassandraWithCredentials.java




