# mpa

**Description** 
This application is a sample of how to generate data, store it to CASSANDRA database and fetch it. 

## FrontEndAPI

**Requirements**

For building and running the application you need:

NodeJS latest version

[Guide d'installation de NodeJS sur Ubuntu](https://www.devroom.io/2011/10/24/installing-node-js-and-npm-on-ubuntu-debian/)

**Running the application locally**

git clone git@gitlab.com:murphys/mpa.git

cd mpa/FrontEndAPI/dataVisualisation

npm i --save

npm start


## BackEndAPI

**Requirements**

For building and running the application you need:

JDK 1.8\
Maven 3

**Running the application locally**

Execute the main method in the com.example.springboot.Application class located in DataGenerator/initial/src/main/java from your IDE.

* Open Eclipse
* File -> Import -> Existing Maven Project -> Navigate to the folder where you unzipped the zip
* Select the project
* Choose the Spring Boot Application file (search for @SpringBootApplication)
* Right Click on the file and Run as Java Application

Alternatively you can use the Spring Boot Maven plugin like so:

mvn spring-boot:run


**Data generator**

To generate more data\
Modify and execute the main method in the com.example.dataGeneratorExample.GenerateDataExample class located in DataGenerator/initial/src/test/java from your IDE.

**URLs**

| URL  | Method |
| ------------- | ------------- |
| http://localhost:8080/api/data?point=Brest  | GET  |
	                                        

**Packages**

model — to hold our entities;

dataGenerator — to hold our business logic;

springboot — to listen to the client;

test/ - contains unit and integration tests

pom.xml - contains all the project dependencies
