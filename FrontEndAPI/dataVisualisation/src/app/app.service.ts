import { Injectable } from '@angular/core';
import { HttpClient, } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
@Injectable()
export class AppService {
    url = 'api/data/';
  constructor(private http: HttpClient) { }
  getData(point:any) {
     return this.http.get(this.url+"?point="+point);
  }
}