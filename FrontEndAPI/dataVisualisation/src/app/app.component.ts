import { Component, NgZone } from '@angular/core';
import * as am4core from "@amcharts/amcharts4/core";
import * as am4charts from "@amcharts/amcharts4/charts";
import am4themes_animated from "@amcharts/amcharts4/themes/animated";
import { AppService } from './app.service'
am4core.useTheme(am4themes_animated);
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'dataVisualisation';

  region: String = "Bretagne"
  point: String = "Renne"
  private chart: am4charts.XYChart;
  private data = [];
  constructor(private zone: NgZone, private appService: AppService) {
  }

  ngAfterViewInit() {
    this.zone.runOutsideAngular(() => {
      let chart = am4core.create("chartdiv", am4charts.XYChart);
      am4core.useTheme(am4themes_animated);

      // auto generated static data

      /* let firstDate = new Date('1990-01-01T10:00:00Z');
      
       firstDate.setDate(firstDate.getDate());
 
       var data = [];
       var value = 50;
       for (let i = -80000; i < 0; i++) {
         let date = new Date();
         let newDate = new Date(firstDate);
         date.setHours(newDate.getHours() + i);
   
         value -= Math.round((Math.random() < 0.5 ? 1 : -1) * Math.random() * 10);
         if (value < 0) {
           value = Math.round(Math.random() * 10);
         }
       
         data.push({ date: date, value: value });
        
       }*/

      // data from database
      this.appService.getData(this.point).subscribe(res => {

        var donnees: any = res
        donnees.forEach(obj => {
          var firstDate = new Date(obj.date)
          firstDate.setDate(firstDate.getDate());
          this.data.push({ date: firstDate, value: obj.value });
        })

        chart.data = this.data;
        console.log(JSON.stringify(this.data))
      }, error => {

        console.log(error);
      });


     
       // Create axes
      var dateAxis = chart.xAxes.push(new am4charts.DateAxis());
      dateAxis.renderer.minGridDistance = 60;
      dateAxis.tooltipDateFormat = "HH:00, d MMMM yyyy";

      // this makes the data to be grouped
      dateAxis.groupData = true;

      var valueAxis = chart.yAxes.push(new am4charts.ValueAxis());

      // Create series
      var series = chart.series.push(new am4charts.LineSeries());
      series.dataFields.dateX = "date";
      series.dataFields.valueY = "value";
      series.tooltipText = "{valueY}";
      series.tooltip.pointerOrientation = "vertical";
      series.tooltip.background.fillOpacity = 0.5;
      series.groupFields.valueY = "average";
      chart.cursor = new am4charts.XYCursor();
      chart.cursor.xAxis = dateAxis;
      chart.cursor.snapToSeries = series;
      var scrollbarX = new am4core.Scrollbar();
      scrollbarX.marginBottom = 20;
      chart.scrollbarX = scrollbarX;
    chart.numberFormatter.numberFormat = "#.##";

    });
  }

  ngOnDestroy() {
    this.zone.runOutsideAngular(() => {
      if (this.chart) {
        this.chart.dispose();
      }
    });
  }
  
}
