package com.example.model;

import java.util.Date;

/**
 * @author mastercassandra
 *
 */
public class Data implements Comparable<Data> {
	private double value;
	private Date date;

	public Data(Date date, double value) {
		super();
		this.value = value;
		this.date = date;
	}

	public double getValue() {
		return value;
	}

	public void setValue(double value) {
		this.value = value;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	@Override
	public int compareTo(Data d) {
		return this.getDate().compareTo(d.getDate());
	}

}
