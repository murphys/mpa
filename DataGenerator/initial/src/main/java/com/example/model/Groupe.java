package com.example.model;

import java.util.List;

/**
 * @author mastercassandra
 *
 */
public class Groupe {

	List<Groupe> groups;
	String name;
	List<Point> points;

	public Groupe(String name, List<Point> points) {
		super();
		this.name = name;
		this.points = points;
	}

	public Groupe(List<Groupe> groups, String name) {
		super();
		this.groups = groups;
		this.name = name;
	}

	public List<Groupe> getGroups() {
		return groups;
	}

	public void setGroups(List<Groupe> groups) {
		this.groups = groups;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<Point> getPoints() {
		return points;
	}

	public void setPoints(List<Point> points) {
		this.points = points;
	}

}
