package com.example.model;

/**
 * @author mastercassandra
 *
 */

public class Point {
	String name;
	Double lon;
	Double lat;

	public Point(String name, Double lon, Double lat) {
		super();
		this.name = name;
		this.lon = lon;
		this.lat = lat;
	}

	public Point(String name) {
		super();
		this.name = name;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Double getLon() {
		return lon;
	}

	public void setLon(Double lon) {
		this.lon = lon;
	}

	public Double getLat() {
		return lat;
	}

	public void setLat(Double lat) {
		this.lat = lat;
	}

}
