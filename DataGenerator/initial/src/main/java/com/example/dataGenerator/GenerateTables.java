package com.example.dataGenerator;

import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import com.example.model.Groupe;
import CQLOperation.ConnectionCassandra;
import CQLOperation.ManagementTable;
import CQLOperation.TypeCassandra;

public class GenerateTables {

	/**
	 * @param name
	 * @param groups
	 * @param connection
	 * @return
	 * @throws Exception
	 */
	public boolean createTable(String name, List<Groupe> groups, ConnectionCassandra connection) throws Exception {

		ManagementTable management = new ManagementTable(connection, true);
		List<String> listPK = new ArrayList<>();
		listPK.add("date");
		listPK.add("point");

		HashMap<String, TypeCassandra> map = new HashMap<>();
		map.put("date", TypeCassandra.TIMESTAMP);
		map.put("point", TypeCassandra.VARCHAR);
		map.put("groupe", TypeCassandra.VARCHAR);
		map.put("data", TypeCassandra.DOUBLE);
		assertTrue(management.createTable(name, listPK, map));

		GenerateData generator = new GenerateData(groups, connection);

		return generator.generateData(name);

	}
}
