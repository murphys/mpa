package com.example.dataGenerator;

import static com.datastax.oss.driver.api.querybuilder.QueryBuilder.bindMarker;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import com.datastax.oss.driver.api.core.cql.Row;
import com.datastax.oss.driver.api.querybuilder.relation.Relation;
import com.example.model.Data;

import CQLOperation.CassandraDB;
import CQLOperation.ConnectionCassandra;
import CQLOperation.SelectionTable;

/**
 * @author mastercassandra
 *
 */
public class RetrieveData {
	
	static SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd'T'hh:mm:ss'Z'");

	/**
	 * @param name
	 * @param point
	 * @return
	 * @throws ParseException
	 */
	public List<Data> getData(String name, String point) throws ParseException {
		CassandraDB cassandraDB = new CassandraDB("Cluster_Lifou", "datacenter1", "releve_data");
		ConnectionCassandra connection = new ConnectionCassandra("172.19.28.182", cassandraDB);
		SelectionTable select = new SelectionTable(connection, true);

		List<String> listC = new ArrayList<>();
		listC.add("date");
		listC.add("point");
		listC.add("groupe");
		listC.add("data");
		// List<Row> rows = select.select(name,listC, false);

		List<Relation> listR = new ArrayList<>();
		List<Object> listV = new ArrayList<>();

		Relation relation = Relation.column("point").isEqualTo(bindMarker());
		listR.add(relation);

		listV.add(point);
		List<Row> rows = select.selectWhere(name, listC, listR, listV, true);
		List<Data> list = new ArrayList<>();

		for (Row r : rows) {
			Data data = new Data(Date.from(r.getInstant(0)), r.getDouble(3));

			list.add(data);
			if (Date.from(r.getInstant(0)).after(formatter.parse("1990-01-02T00:00:00Z"))
					&& Date.from(r.getInstant(0)).before(formatter.parse("1990-01-03T23:00:00Z")))
				System.out.println(r.getInstant(0) + "  |   " + r.getString(1) + " |   " + r.getString(2) + "  |   "
						+ r.getDouble(3));
		}
		System.out.println(rows.size());
		Collections.sort(list);
		return list;
		// return list.subList(0, 80000);
	}
}
