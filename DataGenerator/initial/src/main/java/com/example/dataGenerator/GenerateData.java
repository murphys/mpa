package com.example.dataGenerator;

import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Random;
import java.util.TimeZone;

import com.example.model.Groupe;
import com.example.model.Point;

import CQLOperation.ConnectionCassandra;
import CQLOperation.InsertTable;

/**
 * @author mastercassandra
 *
 */

public class GenerateData {

	static SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd'T'hh:mm:ss'Z'");
	final static String s = "1990-01-01T10:00:00Z";
	final static String e = "2020-02-25T10:00:00Z";

	private ConnectionCassandra connection;

	private List<Groupe> groups;

	public GenerateData(List<Groupe> groups, ConnectionCassandra connection) {
		super();
		this.groups = groups;
		this.connection = connection;

	}

	/**
	 * @param name
	 * @return
	 * @throws Exception
	 */
	public boolean generateData(String name) throws Exception {

		Random ran = new Random();
		try {

			int variationPerMonth = 0;
			int variationParVille = 0;

			for (Groupe g : groups) {

				for (Point p : g.getPoints())

				{
					Date start = formatter.parse(s);
					Date end = formatter.parse(e);

					variationParVille = ran.nextInt(6);

					while (!start.after(end)) {

						Calendar cal = Calendar.getInstance(TimeZone.getTimeZone("Europe/Paris"));
						cal.setTime(start);
						int hour = cal.get(Calendar.HOUR_OF_DAY);
						int day = cal.get(Calendar.DAY_OF_MONTH);
						int month = cal.get(Calendar.MONTH);
						int year = cal.get(Calendar.YEAR);

						if (month == 11 || month == 0 || month == 1)
							variationPerMonth = 5;

						if (month == 3 || month == 4 || month == 5)
							variationPerMonth = 10;

						if (month == 6 || month == 7 || month == 7)
							variationPerMonth = 20;

						if (month == 8 || month == 9 || month == 10)
							variationPerMonth = 10;

						double function = -7 * Math.sin(hour * day * (month + 1) * year) + variationPerMonth
								+ variationParVille;

						// save for an hour
						if (!save(g.getName(), p.getName(), function, start, name))
							return false;
						else
						// start = DateUtils.addHours(start, 1);
						{
							cal.setTime(start);
							cal.add(Calendar.HOUR_OF_DAY, 1);
							start = cal.getTime();
						}

						// start = start.plusDays(1);

					}

				}

			}

		} catch (ParseException ex) {
			ex.printStackTrace();
		}
		return true;
	}

	/**
	 * @param group
	 * @param point
	 * @param data
	 * @param d
	 * @param tableName
	 * @return
	 */
	public boolean save(String group, String point, Double data, Date d, String tableName) {

		DateFormat outputformat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

		InsertTable insert = new InsertTable(connection, true);

		String dateAsString = outputformat.format(d);
		Double val = Double.parseDouble(new DecimalFormat("##.##").format(data));
		List<Object> listValues = new ArrayList<>();
		listValues.add(dateAsString);
		listValues.add(point);
		listValues.add(val);
		listValues.add(group);
		return insert.insertInto(tableName, listValues);

	}

}
