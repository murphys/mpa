package com.example.springboot;

import java.text.ParseException;
import java.util.List;

import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.example.dataGenerator.RetrieveData;
import com.example.model.Data;

/**
 * @author mastercassandra
 *
 */

@RestController
@RequestMapping("/api")
public class HelloController {

	/**
	 * @param point
	 * @return
	 * @throws ParseException
	 */
	@GetMapping(path = "/data", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Object> index(@RequestParam String point) throws ParseException {
		RetrieveData data = new RetrieveData();

		List<Data> entities = data.getData("data_per_hour", point);
		return new ResponseEntity<Object>(entities, HttpStatus.OK);
	}

}
