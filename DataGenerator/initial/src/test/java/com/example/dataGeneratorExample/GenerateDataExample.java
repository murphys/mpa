package com.example.dataGeneratorExample;

import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.List;

import com.example.dataGenerator.GenerateTables;
import com.example.model.Groupe;
import com.example.model.Point;

import CQLOperation.CassandraDB;
import CQLOperation.ConnectionCassandra;
import CQLOperation.ManagementTable;

/**
 * @author mastercassandra
 *
 */
public class GenerateDataExample {

	/**
	 * @param args
	 * @throws Exception
	 */
	public static void main(String[] args) throws Exception {
		CassandraDB cassandraDB = new CassandraDB("Cluster_Lifou", "datacenter1", "releve_data");
		ConnectionCassandra connection = new ConnectionCassandra("172.19.28.182", cassandraDB);
		ManagementTable management = new ManagementTable(connection, true);
		GenerateTables generate = new GenerateTables();
		List<Point> points = new ArrayList<>();
		List<Point> points2 = new ArrayList<>();
		points.add(new Point("Brest"));
		points.add(new Point("Renne"));
		points2.add(new Point("Caen"));
		points2.add(new Point("Rouen"));

		List<Groupe> groups = new ArrayList<>();
		groups.add(new Groupe("Bretagne", points));
		groups.add(new Groupe("Normandie", points2));
		assertTrue(generate.createTable("data_per_hour", groups, connection));

		// assertTrue(management.deleteTable("data_per_hour"));
		connection.closeConnection();

	}

}
